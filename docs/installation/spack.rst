:sequential_nav: next

..  _building_with_spack:

Building Siesta with spack
==========================

`Spack <https://spack.io>`_ is a package manager targeting research
software and supercomputers, although it proves useful even for
software management on a personal machine. It can handle in a
consistent manner the "software stacks" (think of the compiler, the
MPI libraries, optimized linear-algebra libraries, etc).

Please consult Spack documentation for installation instructions.

It is possible to install SIESTA with Spack by using appropriate versions
of the "recipes" used by SIESTA and its dependencies. Typically, these recipes
are kept in the `builtin` repository that is part of the Spack distribution.
However, the Siesta-related recipes in this default repository  might be outdated. 

The procedure involves telling Spack to overlay a custom recipe repository on top
of the default one. The custom repository contains recipes prepared
by the Siesta Project, and is available in the `build-tools` GitLab repo::

    git clone https://gitlab.com/siesta-project/ecosystem/build-tools.git

Navigate to the `Spack` directory and do::

    spack repo add siesta-project-spack-repo  

If you now ask spack about what it knows about package repositories::

    spack repo list

it will answer with something like::

    ==> 2 package repositories.
    siesta-project    /path/to/Spack/siesta-project-spack-repo
    builtin           /path/to/installed/spack/var/spack/repos/builtin

By making sure that the 'siesta-project' repo is listed first in the
spack repository chain we get the right recipes for the build.

After this configuration step, and asuming that the rest of the Spack
installation has been carried out (for definition of compilers, etc) you
can issue commands such as::

     spack install xmlf90
     spack info siesta
     spack spec siesta +mpi +netcdf +libxc +elpa
     spack spec siesta +mpi +netcdf +libxc +elsi ^elsi+enable_pexsi
     spack install siesta -mpi build_type=Debug
     spack install siesta +mpi +netcdf +libxc +elpa ^elpa@2024.03.001 +cuda cuda_arch=80

The last two lines will respectively install a serial version of
Siesta ("-mpi" means "no mpi") with debugging symbols, and a parallel
version of Siesta with NetCDF and Libxc support, and with a
CUDA-enabled modern version of the ELPA library. In both cases the
version installed will be the latest "master" version registered in
the Spack recipe (see the output of "spack info siesta"). To select
other versions you can use the "@version" idiom::

     spack install siesta@5.0.0 +mpi +netcdf +libxc +elpa ^elpa@2024.03.001 +cuda cuda_arch=80

In all cases, the installation will contain all the utility programs in the Siesta distribution.


Making the Spack-compiled versions available for use
----------------------------------------------------

After the "spack install" commands, the executables will be placed in
your spack area, but will not be immediately available in the
PATH. Installed packages might be queried with the "spack find"
command::

    spack find -lv siesta

The "-lv" options will offer a long-form output that includes the features selected in each executable.
    
In order to make the executables available you can run, for example::

    spack load -r siesta@5.0.0    

or use Spack's built-in `modulefiles generation mechanism
<https://spack.readthedocs.io/en/latest/module_file_support.html>`_.

Available Spack versions of SIESTA
----------------------------------

Please run::

  spack info siesta

and look at the first lines for a list of available versions. At present, those include
various tags along the rel-5.0 branch, and versions of the form 'master-{date}', which
belong to the current development branch. For up-to-date information about what is included
in each branch, you can check the `Gitlab wiki page <https://gitlab.com/siesta-project/siesta/-/wikis/Guide-to-Siesta-versions>`_


Use of Spack environments for compilation
-----------------------------------------

Spack environments can be profitably used to build consistent versions of all the dependencies of a package, with particular
options and underlying system libraries.

Please see the spack documentation about environments. In a nutshell::

  mkdir siesta-env
  cd siesta-env
  (copy to this directory the spack.yaml file shown later)
  spack activate .
  spack install

An example `spack.yaml` file for an environment that will install CUDA-enabled Siesta is

.. literalinclude:: spack.yaml


The repo part at the end has to be filled in according to the instructions given in the previous sections.

The environment must be activated in order to use the installed executables (see the spack documentation).










