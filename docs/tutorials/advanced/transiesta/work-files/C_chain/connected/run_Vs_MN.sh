#!/bin/bash
#SBATCH -J tutorialXX
#SBATCH -n 4
#SBATCH -t 0:30:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH -D .

# DO NOT CHANGE THIS LINE
source /gpfs/projects/nct00/nct00003/siestarc.sh

root=$(pwd)

prevV=0

for V in 0.25 0.5 0.75 1; do
        echo Running V=${V} eV

        # Create voltage directory
        mkdir ${root}/${V}V
        cd ${root}/${V}V

        # Copy density matrix, pseudopotentials and inputs from previous runs.
        cp ${root}/${prevV}V/*.{TSDE,psf,fdf} ${root}/${V}V

        # Update the voltage input in the TS.fdf file
        sed -i "/TS.Voltage/c\TS.Voltage ${V} eV" ${root}/${V}V/TS.fdf

        # Run SIESTA
        srun -n 4 siesta < RUN.fdf  > RUN.out

        prevV=${V}
done
